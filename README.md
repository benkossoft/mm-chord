# mm-chord

MM-chord is a simple, peer-to-peer network of logical ring application.
It uses request-reply model with ZMQ.

**Implemented by:**

 * [M.Mucahid Benlioglu](https://github.com/mbenlioglu)
 * [Mert Kosan](https://github.com/mertkosan)


## Getting Started

**Prerequisites:**

- [Python](https://docs.python.org/2/) (developed and tested in python 2.7)
- [pip](https://pip.pypa.io/en/stable/) for python package management

### Installation and Running

Firstly, clone or download this repository, then run the following command in the project root to install needed
dependencies
    
    $ pip install -r requirements.txt

After installation succeeds, you can create a peer node by specifying its address and its successor peer's address with
the following command

    $ python peer.py <self address> <successor address>

This creation is used for initial ring, and assumed to be in the correct order.

A peer can be created without specifying a successor address, however this will force user to enter an address to join
before any command execution, which will create a join request to specified address. Join request places the peer on the
correct place in the ring (based on the assumption that initial order is correct)

    $ python peer.py <self address>
    
Client shell provides basic command completion and history for executed commands, (fully supported on Linux, there are
some bugs in Windows)

For more information you can execute

    $ python peer.py --help

or in the client shell

    > help
    > help <command>
