"""
    Created by mbenlioglu on Apr 10, 2018
"""
import sys
import time
from threading import Thread, Lock
import socket
import zmq
import argparse
import json
from pyfiglet import figlet_format
from cmd import Cmd

try:
    import readline
except ImportError:
    import pyreadline as readline


class _CmdInterface(Cmd):
    def __init__(self, addr, succ, print_func, request_func):
        Cmd.__init__(self)
        self.prompt = '> '
        self.intro = figlet_format('mm - chord')
        self.intro += '''Individual node to compose ring architecture. Type ? or help to see available commands, type
help <command> to learn how command works. Type exit to quit
Your address: %s , Your successor: %s'''
        self._print_func = print_func
        self._request_func = request_func

        self._addr = addr
        self._succ = succ
        self._sock = None
        self.refresh_socket(self._succ)

    def refresh_socket(self, succ):
        if succ is not None:
            try:
                if self._sock is not None:
                    self._sock.disconnect(self._succ)
                else:
                    self._sock = zmq.Context().socket(zmq.REQ)
            except zmq.ZMQError:
                pass
            finally:
                self._sock.connect('tcp://%s' % succ)
        self._succ = succ

    def do_send(self, line):
        """
        Send a message to a destination node, along the ring.

        Parameters:
            ip_address: IPv4 address of the destination node in IP:port format
            message:    Message to be sent to the destination
        """
        line = line.split()
        if len(line) < 2:
            self._print_func('Too few arguments', no_prompt=True)
            self.do_help('send')
            return
        else:
            ip_address = line[0]
            message = ' '.join(line[1:])
        try:
            ip_address = Peer.addr_validate(ip_address, no_prompt=True)
        except ValueError:
            self._print_func('IP Address is not in valid format, message is NOT sent...', no_prompt=True)
            return

        if self._addr == ip_address:
            self._print_func('You cannot send message to yourself', no_prompt=True)
        else:
            payload = {'payload': str(message), 'destination': ip_address, 'source': self._addr}
            res = self._request_func(self._sock, payload)

            if res.get('payload') == 'Acknowledgement':
                self._print_func('Message has been sent along the ring.', no_prompt=True)

    def do_exit(self, line):
        """Exit the shell."""
        self._print_func('Good bye...', no_prompt=True)
        return True


class Peer:
    def __init__(self, addr, succ=None):
        self.addr = self.addr_validate(addr)
        self.succ = succ if succ is None else self.addr_validate(succ)
        self.__print_lock = Lock()
        self.__terminate = False
        self.__cmd_interface = _CmdInterface(self.addr, self.succ, self.__print, self.__request)
        self.__client_thread = Thread(target=self.__client, name='Client Thread')
        self.__server_thread = Thread(target=self.__server, name='Server Thread')

    def __client(self):
        try:
            self.__cmd_interface.cmdloop()
        except KeyboardInterrupt:
            self.__print('Good bye...', no_prompt=True)
        self.__print('Halting threads and exiting...', no_prompt=True)
        self.__terminate = True
        return

    def __server(self):
        context = zmq.Context()

        # Reply
        rep_addr = "tcp://{}".format(self.addr)
        sock_rep = context.socket(zmq.REP)
        sock_rep.bind(rep_addr)

        # Forward
        forward_addr = "tcp://{}".format(self.succ)
        sock_forward = context.socket(zmq.REQ)
        sock_forward.connect(forward_addr)

        while not self.__terminate:
            try:
                message = json.loads(sock_rep.recv(flags=zmq.NOBLOCK))
            except zmq.ZMQError:
                time.sleep(0.1)
                continue

            if message.get('destination') == self.addr:
                # If destination is me
                sock_rep.send(json.dumps({'payload': "Acknowledgement"}))

                self.__print(
                    "new packet: " + message.get('payload'),
                    "source: " + message.get('source'),
                    "destination: " + message.get('destination'),
                )

                source = message.get('source')

                source_addr = "tcp://{}".format(source)
                sock_source = context.socket(zmq.REQ)
                sock_source.connect(source_addr)

                ack_msg = self.__request(sock_source, {'message_delivered': True,
                                                       'destination': self.addr,
                                                       'source': source})
                sock_source.close()
            elif message.get('message_delivered') is not None:
                # If my message is delivered
                sock_rep.send(json.dumps({'payload': "Acknowledgement"}))

                self.__print("packet sent",
                             "destination: " + message.get('destination'),
                             "source: " + message.get('source'),
                             "status: OK")

            elif message.get('is_join') is not None:
                # If join request is came
                sock_rep.send(json.dumps({'payload': "Acknowledgement"}))

                new_addr = message.get('new_address')
                new_node_id = int(new_addr.split(":")[1])
                current_node_id = int(self.addr.split(":")[1])
                succ_node_id = int(self.succ.split(":")[1])

                if (succ_node_id > new_node_id > current_node_id) or \
                        (new_node_id > current_node_id > succ_node_id) or \
                        (current_node_id > succ_node_id > new_node_id):
                    # position is found
                    self.__print("{} is trying to join ring, position found!".format(new_addr))

                    new_peer_addr = "tcp://{}".format(new_addr)
                    sock_new_peer = context.socket(zmq.REQ)
                    sock_new_peer.connect(new_peer_addr)

                    ack_msg = self.__request(sock_new_peer, {'is_join': True,
                                                             'new_address': new_addr,
                                                             'successor': self.succ})
                    sock_new_peer.close()

                    # new forward address = new successor
                    self.succ = new_addr
                    sock_forward.disconnect(forward_addr)
                    forward_addr = "tcp://{}".format(self.succ)
                    sock_forward.connect(forward_addr)

                    self.__print("New successor: {}".format(self.succ))
                else:
                    # position not found look successor
                    self.__print("{} is trying to join ring, position is not found, forwarding...".format(new_addr))
                    ack_msg = self.__request(sock_forward, message)
            else:
                # If payload message will be forwarded
                sock_rep.send(json.dumps({'payload': "Acknowledgement"}))

                self.__print("new packet: " + message.get('payload'),
                             "source: " + message.get('source'),
                             "destination: " + message.get('destination'),
                             "\nWe are not interested in this packet, sending to successor")
                ack_msg = self.__request(sock_forward, message)

    def __join_ring(self, addr):
        context = zmq.Context()

        # Join Req
        join_addr = "tcp://{}".format(addr)
        sock_join = context.socket(zmq.REQ)
        sock_join.connect(join_addr)

        # Join Rep
        join_new_addr = "tcp://{}".format(self.addr)
        sock_join_done = context.socket(zmq.REP)
        sock_join_done.bind(join_new_addr)

        ack_msg = self.__request(sock_join, {'is_join': True,
                                             'new_address': self.addr})

        response = json.loads(sock_join_done.recv())
        self.succ = response.get('successor')
        self.__cmd_interface.refresh_socket(self.succ)
        sock_join_done.send(json.dumps({'payload': "Acknowledgement"}))

    @staticmethod
    def __request(sock, payload):
        sock.send(json.dumps(payload))
        return json.loads(sock.recv())

    def __print(self, *args, **kwargs):
        with self.__print_lock:
            line = readline.get_line_buffer()
            sys.stdout.write('\n' + ' '.join(args) + '\n')
            if not self.__terminate and not kwargs.get('no_prompt'):
                sys.stdout.write(self.__cmd_interface.prompt + line)
            sys.stdout.flush()

    @staticmethod
    def addr_validate(addr, no_prompt=False):
        while True:
            try:
                ip, port = addr.split(':')
                socket.inet_aton(ip)
                port = int(port)
                if port < 0:
                    raise ValueError
            except (socket.error, ValueError):
                msg = 'Address is not valid (IP:port)'
                if not no_prompt:
                    addr = raw_input(msg + ' please enter a valid address: ')
                    continue
                else:
                    raise ValueError(msg)
            break
        return '%s:%d' % (ip, port)

    def start(self, join_addr=None):
        if self.succ is None:
            print 'Peer started without a defined successor...'
            if join_addr is None:
                prompt = '''Please enter the successor address to join a logical ring
                \nJOIN ADDRESS: '''
                join_addr = self.addr_validate(raw_input(prompt))
            print 'Sending join request to %s ...\n' % join_addr
            self.__join_ring(join_addr)
        self.__cmd_interface.intro = self.__cmd_interface.intro % (self.addr, self.succ)
        self.__server_thread.start()
        self.__client_thread.start()

    def join(self):
        self.__client_thread.join()
        self.__server_thread.join()


if __name__ == '__main__':
    _intro = '%(prog)s summons a node that can form a logical ring with other such nodes.'
    _help_peer_addr = 'IP and port of the peer that will be created.'
    _help_peer_succ = 'IP and port of the successor of the peer that will be created. (Optional)'
    _help_join = 'Join an existing ring with a known peer IP address. This option will be ignored if successor ' \
                 'address is also specified.'


    def _valid_addr(addr):
        try:
            addr = Peer.addr_validate(addr, True)
        except ValueError as e:
            raise argparse.ArgumentTypeError(str(e))
        return addr


    cmd_parser = argparse.ArgumentParser(description=_intro)
    cmd_parser.add_argument('peer_addr', metavar='ADDR', type=_valid_addr, help=_help_peer_addr)
    cmd_parser.add_argument('succ_addr', nargs='?', metavar='SUCC', type=_valid_addr,
                            help=_help_peer_succ)
    cmd_parser.add_argument('-j', '--join_ring', metavar='ADDR', type=_valid_addr, help=_help_join)

    cmd_args = cmd_parser.parse_args()

    peer = Peer(cmd_args.peer_addr, cmd_args.succ_addr)
    if cmd_args.join_ring is not None:
        peer.start(join_addr=cmd_args.join_ring)
    else:
        peer.start()

    peer.join()
